<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function cariuser($uid)
	{
		$this->db->select('*');
		$this->db->from('transaksi a');
		$this->db->join('detailtransaksi b', 'a.no_transaksi = b.kd_transaksi');
		$this->db->where('a.user', $uid);
		$this->db->where('YEAR(a.createdate) <=', date('Y-m-d'));
		$this->db->order_by('a.id_transaksi', 'desc');
		$this->db->limit(1);
		return $this->db->get();
	}

	function userAvailable($uid)
	{
		$this->db->where('id_user', $uid);
		return $this->db->get('user', 1);
	}

	function activate($token)
	{
		$this->db->where('no_transaksi', $token);
		return $this->db->get('transaksi', 1);
	}

	function getCurrentToken($uid)
	{
		$this->db->where('id_user', $uid);
		return $this->db->get('currentquota', 1);
	}

}

/* End of file Admin_model.php */
/* Location: ./application/models/Admin_model.php */