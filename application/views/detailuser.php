<script type="text/javascript">
  function purchase (uid) {
    $('#cont').load('<?php echo base_url('adminboard/purchasetoken/') ?>'+uid);
  }
</script>

<script type="text/javascript" src="<?= base_url('assets/js/jquery.mask.min.js') ?>"></script>

<div class="form-panel">
  <h4 class="mb">
    <i class="fa fa-angle-right"></i> Detail User's Transaction
    <button class="btn btn-success pull-right" data-toggle="modal" onclick="purchase(<?= $username ?>)" data-target="#addmodal">
      Purchase Token
    </button>
  </h4>

  <?php 
    if ($last->result()) {
      $l = $last->row()->listrik;
      $a = $last->row()->air;
      $g = $last->row()->gas;
      $n = $last->row()->none;
      $date = $res->row()->createdate;
    } else {
      $l = 0;
      $a = 0;
      $g = 0;
      $n = 0;
      $date = '-';
    }
  ?>

  <form class="form-horizontal style-form" method="">
      <div class="form-group">
          <label class="col-lg-2 col-sm-2 control-label">User ID</label>
          <div class="col-lg-10">
              <input type="text" class="form-control" value="<?= $username; ?>" readonly/>
          </div>

          <br><br>

          <label class="col-lg-2 col-sm-2 control-label">User Name</label>
          <div class="col-lg-10">
              <input type="text" class="form-control" value="<?= getUser($username); ?>" readonly/>
          </div>

          <br><br>

          <label class="col-lg-2 col-sm-2 control-label">Last Transaction</label>
          <div class="col-lg-10">
              <input type="text" class="form-control" value="<?= $date; ?>" readonly/>
          </div>
      </div>

      <div class="form-group">
          <label class="col-lg-2 col-sm-2 control-label">Listrik</label>
          <div class="col-lg-10">
              <input type="text" class="form-control" value="<?= $l; ?>" readonly/>
          </div>

          <br><br><br>
          
          <label class="col-lg-2 col-sm-2 control-label">Air</label>
          <div class="col-lg-10">
              <input type="text" class="form-control" value="<?= $a; ?>" readonly/>
          </div>
          
          <br><br><br>
          
          <label class="col-lg-2 col-sm-2 control-label">Gas</label>
          <div class="col-lg-10">
              <input type="text" class="form-control" value="<?= $g; ?>" readonly/>
          </div>
          
          <br><br><br>
          
          <label class="col-lg-2 col-sm-2 control-label">None</label>
          <div class="col-lg-10">
              <input type="text" class="form-control" value="<?= $n; ?>" readonly/>
          </div>
      </div>
  </form>
</div>

<div class="modal fade" id="addmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" id="cont">
     
    </div>
  </div>
</div>