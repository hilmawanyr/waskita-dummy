<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>DASHGUM - Bootstrap Admin Template</title>

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url() ?>assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?= base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/css/style-responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lineicons/style.css"> 
    <script src="<?= base_url() ?>assets/js/jquery.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  	<section id="container" >
      	<header class="header black-bg">
          	<div class="sidebar-toggle-box">
                <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
            </div>

            <!--logo start-->
            <a href="index.html" class="logo"><b>Brand</b></a>
            <!--logo end-->
           
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="login.html">Logout</a></li>
            	</ul>
            </div>
        </header>

      	<aside>
          	<div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              	<ul class="sidebar-menu" id="nav-accordion">
              
              	  	<p class="centered"><a href="profile.html"><img src="<?= base_url() ?>assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
              	  	<h5 class="centered">IDEword</h5>
              	  	
                  	<li class="mt">
                      	<a href="<?= base_url('userboard') ?>" class="active">
                          	<i class="fa fa-user"></i>
                          	<span>User Board</span>
                      	</a>
                  	</li>

                  	<li class="sub-menu">
                      	<a href="<?= base_url('adminboard') ?>">
                          	<i class="fa fa-key"></i>
                          	<span>Admin Board</span>
                      	</a>
                  	</li>

              	</ul>
              	<!-- sidebar menu end-->
          	</div>
      	</aside>

      	<section id="main-content">
          	<section class="wrapper site-min-height">
	          	<?php $this->load->view($page); ?>
			</section><!--/wrapper -->
      	</section><!-- /MAIN CONTENT -->

      	<footer class="site-footer">
          	<div class="text-center">
              	<?php echo date('Y') ?> - Alvarez.is
              	<a href="blank.html#" class="go-top">
                  	<i class="fa fa-angle-up"></i>
              	</a>
          	</div>
      	</footer>
  	</section>

    <!-- js placed at the end of the document so the pages load faster -->
    
    <script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="<?= base_url() ?>assets/js/jquery.ui.touch-punch.min.js"></script>
    <script class="include" type="text/javascript" src="<?= base_url() ?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?= base_url() ?>assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?= base_url() ?>assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="<?= base_url() ?>assets/js/common-scripts.js"></script>

    <!--script for this page-->
    
  <script>
      //custom select box

      // $(function(){
      //     $('select.styled').customSelect();
      // });

  </script>

  </body>
</html>
