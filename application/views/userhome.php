<script type="text/javascript">
	$(document).ready(function(){
		setInterval(function(){
			$.post('<?= base_url('userboard/getLastQuota/1/l') ?>', function(response){
				$('#bolt').html(response);
			});

			$.post('<?= base_url('userboard/getLastQuota/1/a') ?>', function(response){
				if (response < 5) {
					$.post('<?= base_url('userboard/equalization/1/a') ?>', function(res){
						$('#water').html(res);
					});
				} else {
					$('#water').html(response);
				}
			});

			$.post('<?= base_url('userboard/getLastQuota/1/g') ?>', function(response){
				if (response < 5) {
					$.post('<?= base_url('userboard/equalization/1/g') ?>', function(res){
						$('#gas').html(res);
					});
				} else {
					$('#gas').html(response);
				}
			});

			$.post('<?= base_url('userboard/getLastQuota/1/n') ?>', function(response){
				if (response < 5) {
					$.post('<?= base_url('userboard/equalization/1/n') ?>', function(res){
						$('#what').html(res);
					});
				} else {
					$('#what').html(response);
				}
			});
		}, 3000);
	});
</script>

<div class="row mt">
	<div class="col-lg-12">
		<!-- -- 6TH ROW OF PANELS ---->
		<div class="row">
			
			<div class="col-lg-3 col-md-3 col-sm-3 mb">
				<div class="weather-3 pn centered">
					<i class="fa fa-bolt"></i>
					<h1 id="bolt"></h1>
					<div class="info">
						<div class="row">
							<h3 class="centered">Listrik</h3>
							<div class="col-sm-6 col-xs-6 pull-left">
								<p class="goleft"><i class="fa fa-tint"></i> 13%</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-md-3 col-sm-3 mb">
				<div class="weather-3 pn centered">
					<i class="fa fa-tint"></i>
					<h1 id="water"></h1>
					<div class="info">
						<div class="row">
								<h3 class="centered">Air</h3>
							<div class="col-sm-6 col-xs-6 pull-left">
								<p class="goleft"><i class="fa fa-tint"></i> 13%</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-md-3 col-sm-3 mb">
				<div class="weather-3 pn centered">
					<i class="fa fa-tachometer"></i>
					<h1 id="gas"></h1>
					<div class="info">
						<div class="row">
								<h3 class="centered">Gas</h3>
							<div class="col-sm-6 col-xs-6 pull-left">
								<p class="goleft"><i class="fa fa-tint"></i> 13%</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<!-- WEATHER-3 PANEL -->
			<div class="col-lg-3 col-md-3 col-sm-3 mb">
				<div class="weather-3 pn centered">
					<i class="fa fa-database"></i>
					<h1 id="what"></h1>
					<div class="info">
						<div class="row">
								<h3 class="centered">Unknown</h3>
							<div class="col-sm-6 col-xs-6 pull-left">
								<p class="goleft"><i class="fa fa-tint"></i> 13%</p>
							</div>
						</div>
					</div>
				</div>
			</div><!-- --/col-md-4 ---->
		</div><!-- /END 6TH ROW OF PANELS -->

		<div class="form-panel">
         	<form class="form-horizontal style-form" method="post" action="<?= base_url('userboard/activationToken') ?>">
         		<h4><i class="fa fa-angle-right"></i> Top up token</h4>
              	<div class="form-group">
                  	<label class="col-sm-2 col-sm-2 control-label">Token Number</label>
                  	<div class="col-sm-9">
                      	<input class="form-control" type="text" name="key">
                  	</div>
                  	<button type="submit" class="btn btn-success"><i class="fa fa-check"></i></button>
              	</div>
         	</form>
      	</div>
	</div>
</div>