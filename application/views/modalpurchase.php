<script type="text/javascript">
  $(document).ready(function() {
     $('#money,#money2').mask('000.000.000.000.000', {reverse: true});
  });
</script>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
  <h4 class="modal-title">Purchase Token</h4>
</div>

<div class="modal-body">

  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Package</a></li>
    <li><a data-toggle="tab" href="#menu1">Modular</a></li>
  </ul>

  <div class="tab-content">

    <div id="home" class="tab-pane fade in active">
      <h3>Purchase Package</h3>
      <form class="form-horizontal" action="<?= base_url('adminboard/savePurchasePackage') ?>" method="post">
        <div class="form-group">
          <label class="col-lg-2 col-sm-2 control-label">Amount</label>
          <div class="col-lg-10">
            <input type="text" id="money" class="form-control" name="uang"/>
            <input type="hidden" value="<?= $user; ?>" name="uid">
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>

    <div id="menu1" class="tab-pane fade">
      <h3>Purchase per Modul</h3>
      <form class="form-horizontal" action="<?= base_url('adminboard/savePurchaseModular') ?>" method="post">
        <div class="form-group">
          <label class="col-lg-2 col-sm-2 control-label">Category</label>
          <div class="col-lg-10">
            <select class="form-control" name="category">
              <option value="l">Listrik</option>
              <option value="a">Air</option>
              <option value="g">Gas</option>
              <option value="n">None</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="col-lg-2 col-sm-2 control-label">Amount</label>
          <div class="col-lg-10">
            <input type="text" id="money2" class="form-control" name="uang2"/>
            <input type="hidden" value="<?= $user; ?>" name="uidd">
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>