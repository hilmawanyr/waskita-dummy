<script type="text/javascript">
  function cari () {
    var user = $('#user').val();
    $('#showhere').load('<?php echo base_url('adminboard/caridata/') ?>'+user);
  }
</script>

<div class="col-lg-12">
	<div class="form-panel">
    <h4 class="mb"><i class="fa fa-angle-right"></i> Search User</h4>
      <div class="form-group">
          <label class="col-lg-2 col-sm-2 control-label">User ID</label>
          <div class="col-lg-9">
              <input type="text" class="form-control" id="user" placeholder="Masukan ID Pengguna">
          </div>
          <button class="btn btn-success" onclick="cari()"><i class="fa fa-search"></i></button>
      </div>
  </div>

  <div id="showhere">

  </div>
</div>