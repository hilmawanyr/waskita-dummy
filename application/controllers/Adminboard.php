<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminboard extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model','admin');
	}

	public function index()
	{
		$data['page'] = 'adminhome';
		$this->load->view('template', $data);
	}

	function caridata($uid)
	{
		$data['username'] = $uid;
		$data['res'] = $this->admin->cariuser($uid);

		$data['last'] = $this->admin->getCurrentToken($uid);

		$anyuser = $this->admin->userAvailable($uid)->result();

		if ($anyuser) {
			$this->load->view('detailuser', $data);
		} else {
			echo '<div class="form-panel">
					<h1 class="mb">
    					:( 
    				</h1>
    				<h4>User was not found</h4>
    			  </div>';
		}
	}

	function purchasetoken($uid)
	{
		$data['user'] = $uid;
		$this->load->view('modalpurchase', $data);
	}

	function savePurchasePackage()
	{
		$amount = $this->input->post('uang', TRUE);
		$userid = $this->input->post('uid', TRUE);

		$replace= str_replace('.', '', $amount);

		// anggap 1 Kwh = Rp.1000,-
		$constan= 1000;

		// 25% dibagi untuk masig-masing device
		$persen = ($replace*25)/100;

		// guna mendapat nilai satuan Kwh yang didapat
		$divide = $persen/$constan;

		$check	= $this->admin->cariuser($userid);
		$row 	= $check->row();

		// jika sebelumnya sudah pernah top up
		if ($check->result()) {

			// apakah sudah pernah aktivasi
			$hasactivate = $this->admin->getCurrentToken($userid);

			if ((bool)$hasactivate->result()) {
			
				$data['listrik'] 	= ($divide*1)+number_format($hasactivate->row()->listrik);
				$data['air'] 		= ($divide*1)+number_format($hasactivate->row()->air);
				$data['gas'] 		= ($divide*1)+number_format($hasactivate->row()->gas);
				$data['none'] 		= ($divide*1)+number_format($hasactivate->row()->none);

			} else {
				
				$data['listrik'] 	= ($divide*1)+number_format($row->listrik);
				$data['air'] 		= ($divide*1)+number_format($row->air);
				$data['gas'] 		= ($divide*1)+number_format($row->gas);
				$data['none'] 		= ($divide*1)+number_format($row->none);

			}

		// jika belum pernah top up (pengisian pertama)
		} else {
			
			$data['listrik'] 	= $divide*1;
			$data['air'] 		= $divide*1;
			$data['gas'] 		= $divide*1;
			$data['none'] 		= $divide*1;

		}

		// insert to detail transaksi
		$data['kd_transaksi'] = md5($userid.date('Y-m-d H:i:s'));
		$this->db->insert('detailtransaksi', $data);

		// insert to transaksi
		$trk['user']		= $userid;
		$trk['harga']		= $replace;
		$trk['no_transaksi']= $data['kd_transaksi'];
		$trk['createdate']	= date('Y-m-d H:i:s');
		$trk['admin']		= '';

		$this->db->insert('transaksi', $trk);
		
		echo "<script>alert('Berhasil!');history.go(-1);</script>";
		
	}

	function savePurchaseModular()
	{
		$user = $this->input->post('uidd', TRUE);
		$uang = $this->input->post('uang2', TRUE);
		$repl = str_replace('.', '', $uang);
		$getinfo = $this->admin->cariuser($user);

		// jika pembelian perdana
		if (!$getinfo->result()) {
			echo "<script>alert('Pembelian per modul hanya bisa dilakukan setelah pembelian perdana!');history.go(-1);</script>";

		// jika bukan pembelian perdana
		} else {
			
			$category = $this->input->post('category', TRUE);

			// cek quota terakhir
			$getlasttoken = $this->admin->getCurrentToken($user)->row();

			// jika bukan pembelian token listrik
			if ($category != 'l') {
				
				// cek quota listrik terakhir
				if ($getlasttoken->listrik < 5) {
					echo "<script>alert('Token listrik anda kurang dari 5 Kwh, harap mengisi token listrik anda terlebih dahulu!');history.go(-1);</script>";
				} else {
					
					// get transaksi terakhir
					$last = $this->admin->cariuser($user);

					// insert to transaksi
					$data['user'] = $user;
					$data['harga'] = $repl;
					$data['no_transaksi'] = md5($user.date('Y-m-d H:i:s'));
					$data['createdate']	= date('Y-m-d H:i:s');

					$this->db->insert('transaksi', $data);

					// hitung quota berdasarkan nominal pembelian
					$const = 1000;
					$reslt = $repl/$const;

					// insert to detail transaksi
					if ($category == 'a') {
						
						$detl['gas'] = $getlasttoken->gas; 
						$detl['air'] = number_format($getlasttoken->air)+$reslt; 
						$detl['listrik'] = $getlasttoken->listrik; 
						$detl['none'] = $getlasttoken->none; 
						$detl['kd_transaksi'] = $data['no_transaksi'];

					} elseif ($category == 'g') {
						
						$detl['gas'] = number_format($getlasttoken->gas)+$reslt; 
						$detl['air'] = $getlasttoken->air;
						$detl['listrik'] = $getlasttoken->listrik; 
						$detl['none'] = $getlasttoken->none; 
						$detl['kd_transaksi'] = $data['no_transaksi'];

					} else {
						
						$detl['gas'] = $getlasttoken->gas;
						$detl['air'] = $getlasttoken->air;
						$detl['listrik'] = $getlasttoken->listrik; 
						$detl['none'] = number_format($getlasttoken->none)+$reslt;
						$detl['kd_transaksi'] = $data['no_transaksi'];

					}

					$this->db->insert('detailtransaksi', $detl);
					/*
					// update currentquota
					if ($category == 'a') {
						
						$arr = [
							'air' =>  number_format($getlasttoken->air)+$reslt, 
							'kd_trk' => $data['no_transaksi']
						];

					} elseif ($category == 'g') {
						
						$arr = [
							'gas' =>  number_format($getlasttoken->gas)+$reslt, 
							'kd_trk' => $data['no_transaksi']
						];

					} else {
						
						$arr = [
							'none' =>  number_format($getlasttoken->none)+$reslt, 
							'kd_trk' => $data['no_transaksi']
						];

					}
					$this->db->where('id_user', $user);
					$this->db->update('currentquota', $arr);
					*/
					
					echo "<script>alert('Top Up Berhasil!');history.go(-1);</script>";
				}
				
			} else {
				
				$cons = 1000;
				$purc = $repl/$cons;

				// insert to transaksi
				$data['user'] = $user;
				$data['harga'] = $repl;
				$data['no_transaksi'] = md5($user.date('Y-m-d H:i:s'));
				$data['createdate']	= date('Y-m-d H:i:s');

				$this->db->insert('transaksi', $data);

				// get last transaction
				$getlasttoken = $this->admin->getCurrentToken($user)->row();

				// insert to detailtransaksi
				$detl['gas'] = $getlasttoken->gas;
				$detl['air'] = $getlasttoken->air;
				$detl['listrik'] = number_format($getlasttoken->listrik)+$purc; 
				$detl['none'] = $getlasttoken->none; 
				$detl['kd_transaksi'] = $data['no_transaksi'];

				$this->db->insert('detailtransaksi', $detl);

				/*
				// update currenttoken
				$arr = [
						'listrik' =>  number_format($getlasttoken->listrik)+$purc, 
						'kd_trk' => $data['no_transaksi']
					];

				$this->db->update('currentquota', $arr);
				*/
				echo "<script>alert('Top Up Berhasil!');history.go(-1);</script>";
			}
		}
		
	}

}

/* End of file Adminboard.php */
/* Location: ./application/controllers/Adminboard.php */