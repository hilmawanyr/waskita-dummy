<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userboard extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model','admin');
	}

	public function index()
	{	
		$data['page'] = 'userhome';
		$this->load->view('template', $data);
	}

	function getLastQuota($uid,$type)
	{
		$data = $this->admin->getCurrentToken($uid);

		if ($data->result()) {
			
			if ($type == 'l') {
				echo $data->row()->listrik;
			} elseif ($type == 'a') {
				echo $data->row()->air;
			} elseif ($type == 'g') {
				echo $data->row()->gas;
			}elseif ($type == 'n') {
				echo $data->row()->none;
			}

		} else {
			echo 0;
		}
		
	}

	function activationToken()
	{
		// $user 	= $this->session->userdata('sesslogin');
		$user['id_user'] = 1;
		$token 	= $this->input->post('key', TRUE);

		// cek apakah token terdaftar
		$check 	= $this->admin->activate($token);

		// jika token terdaftar
		if ($check->result()) {

			// get last transaksi
			$getQuota = $this->admin->cariuser($user['id_user'])->row();
			$arr['id_user'] = $user['id_user'];
			$arr['listrik']	= $getQuota->listrik;
			$arr['air'] 	= $getQuota->air;
			$arr['gas'] 	= $getQuota->gas;
			$arr['none'] 	= $getQuota->none;
			$arr['kd_trk']	= $getQuota->no_transaksi;
			
			// apakah pengisian perdana
			$checkagain = $this->admin->getCurrentToken($user['id_user']);

			// jika bukan pengisian perdana
			if ($checkagain->result()) {
				
				// update
				$this->db->where('id_user', $user['id_user']);
				$this->db->update('currentquota', $arr);

				// update status pada transaksi
				$arry = ['status' => 1];
				$this->db->where('no_transaksi', $token);
				$this->db->update('transaksi', $arry);

			// jika pengisian perdana
			} else {
				
				// insert
				$this->db->insert('currentquota', $arr);

				// update status pada transaksi
				$arry = ['status' => 1];
				$this->db->where('no_transaksi', $token);
				$this->db->update('transaksi', $arry);

			}

			echo "<script>alert('Aktivasi berhasil!');history.go(-1);</script>";
			
		// jika token belum terdaftar
		} else {
			echo "<script>alert('Token tidak terdaftar!');history.go(-1);</script>";
		}
		
	}

	function equalization($uid,$type)
	{
		// get last quota
		$getbolt = $this->admin->getCurrentToken($uid);

		// check amount of last electric quota
		$bolt = $getbolt->row()->listrik;

		// if last quota of electricity more than 5, share 25% of its rest to other
		if ($bolt > 5) {

			// get rest after reduce by 5
			$rest = $bolt - 5;
			$shar = ($rest*25)/100;

			if ($type == 'a') {
				
				// distribute qouta to other
				$add = $getbolt->row()->air;
				$res = $add + $shar;

				// reduce quota of bolt
				$blt = $bolt - $shar;
				$arr = ['air' => $res, 'listrik' => $blt];
				$this->db->where('id_user', $uid);
				$this->db->update('currentquota', $arr);

			} elseif ($type == 'g') {
				
				// distribute qouta to other
				$add = $getbolt->row()->gas;
				$res = $add + $shar;

				// reduce quota of bolt
				$blt = $bolt - $shar;
				$arr = ['gas' => $res, 'listrik' => $blt];
				$this->db->where('id_user', $uid);
				$this->db->update('currentquota', $arr);

			} elseif ($type == 'n') {
				
				// distribute qouta to other
				$add = $getbolt->row()->none;
				$res = $add + $shar;

				// reduce quota of bolt
				$blt = $bolt - $shar;
				$arr = ['none' => $res, 'listrik' => $blt];
				$this->db->where('id_user', $uid);
				$this->db->update('currentquota', $arr);

			}			
		}
	}

}

/* End of file Userboard.php */
/* Location: ./application/controllers/Userboard.php */